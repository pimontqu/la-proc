<?php

class User extends Database{

    public $id;
    public $email;
    public $password;
    public $password_reset;
    public $id_roles;
    public $role;

    public function __construct($info_db){
        parent::__construct($info_db);
    }
    // verifie si l'utilisateur existe
    public function exit($email){
        $exit = $this->dbConnect->prepare('SELECT COUNT(*) as exist FROM la_users WHERE email = :email');
        $exit->bindValue(':email', $email, PDO::PARAM_STR);
        $exit->execute();
        return $exit->fetch(PDO::FETCH_OBJ)->exist;
    }
    // ajoute un utilisateur
    public function add(){
        $query = 'INSERT INTO la_users(email, password, password_reset, id_la_roles) VALUES(:email, :pwd, FALSE, :id_roles)';
        $add_user = $this->dbConnect->prepare($query);
        $add_user->bindValue(':email', $this->email, PDO::PARAM_STR);
        $add_user->bindValue(':pwd', $this->password, PDO::PARAM_STR);
        $add_user->bindValue(':id_roles', $this->id_roles, PDO::PARAM_INT);
        return $add_user->execute();
    }
    // recupère un utilisateur
    public function get($email){
        $is_ok = false;
        $query = 'SELECT la_users.id AS ident, email, password, la_roles.name AS user_role 
        FROM la_users 
        INNER JOIN la_roles ON la_roles.id = la_users.id_la_roles WHERE email = :email';
        $get_user = $this->dbConnect->prepare($query);
        $get_user->bindValue(':email',  $email, PDO::PARAM_STR);
        if($get_user->execute()){
            $response = $get_user->fetch(PDO::FETCH_OBJ);
            $this->id = $response->ident;
            $this->email = $response->email;
            $this->password = $response->password;
            $this->role = $response->user_role;
            $is_ok = true;
        }
        return $is_ok;
        
    }
    // supprime un utilisateur et tous ces abonnement
    public function remove($id){
        $query = 'DELETE FROM la_users WHERE id = :id';
        $delete = $this->dbConnect->prepare($query);
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        $query_sub = 'DELETE FROM la_subscriptions WHERE id_la_users = :id_user';
        $delete_sub = $this->dbConnect->prepare($query_sub);
        $delete_sub->bindValue(':id_user', $id, PDO::PARAM_INT);
        $this->dbConnect->beginTransaction();
        if($delete_sub->execute()){
            if($delete->execute()){
                $this->dbConnect->commit();
                return true;
            }else{
                $this->dbConnect->rollback();
                return false;
            }
        }else{
            $this->dbConnect->rollback();
            return false;
        }
        return $delete->execute();
    }

    // récupère les utilisateurs
    public function get_all(){
        $query = 'SELECT la_users.id AS user_id, email, la_roles.name AS user_role 
        FROM la_users 
        INNER JOIN la_roles ON la_roles.id = la_users.id_la_roles';
        $get_user = $this->dbConnect->query($query);
        return $get_user->fetchAll(PDO::FETCH_OBJ);
        
    }
}