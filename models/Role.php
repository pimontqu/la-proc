<?php

class Role extends Database{
    public $id;
    public $name;

    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    public function add($name){
        $query = 'INSERT INTO la_roles(name) VALUES(:name)';
        $add = $this->dbConnect->prepare($query);
        $add->bindValue(':name', $name, PDO::PARAM_STR);
        return $add->execute();
    }

    public function get($name){

    }

    public function get_id($name){
        $is_ok = false;
        $query = 'SELECT id FROM la_roles WHERE name = :name';
        $get_id = $this->dbConnect->prepare($query);
        $get_id->bindValue(':name', $name, PDO::PARAM_STR);
        if($get_id->execute()){
            $result = $get_id->fetch(PDO::FETCH_OBJ);
            $this->id = $result->id;
            $is_ok = true;
        }
        
    }

    public function exist($name){
        $query = 'SELECT COUNT(*) as exist FROM la_roles WHERE name = :name';
        $exist = $this->dbConnect->prepare($query);
        $exist->bindValue(':name', $name, PDO::PARAM_STR);
        $exist->execute();
        return $exist->fetch(PDO::FETCH_OBJ)->exist;
    }
}