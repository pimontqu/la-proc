<?php

class Moodledb{
    protected $host;
    protected $pwd;
    protected $login;
    protected $db;
    protected $dbConnect;

    /**
     * recupère toutes les informations nécessaire pour la connexion et ce que connecte à la base de données
     */
    public function __construct($db_info) {
        $this->host = $db_info['host'];
        $this->db = $db_info['name'];
        $this->login = $db_info['user'];
        $this->pwd = $db_info['pwd'];
        try {
            $this->dbConnect = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db . ';charset=utf8', $this->login, $this->pwd);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    // récupère l'id de l'utilisateur dans moodle
    public function get_id_user($email){
        $query = 'SELECT id FROM mdl_user WHERE email = :email';
        $get_id = $this->dbConnect->prepare($query);
        $get_id->bindValue(':email', $email, PDO::PARAM_STR);
        $get_id->execute();
        return $get_id->fetch(PDO::FETCH_OBJ)->id;
    }
    // récupère les cours de l'utilisateurs 
    public function get_courses($id){
        $query = 'SELECT mdl_course.id AS course_id, mdl_course.fullname AS course_name
        FROM mdl_context 
        INNER JOIN mdl_course ON instanceid  = mdl_course.id AND contextlevel = 50 
        INNER JOIN mdl_role_assignments ON mdl_context.id = mdl_role_assignments.contextid 
        INNER JOIN mdl_user ON mdl_user.id = mdl_role_assignments.userid 
        WHERE mdl_user.id = :id AND mdl_role_assignments.roleid IN (3, 4)';
        $get_course = $this->dbConnect->prepare($query);
        $get_course->bindValue(':id', $id, PDO::PARAM_INT);
        $get_course->execute();
        return $get_course->fetchAll(PDO::FETCH_OBJ);
    }
    // récupère les devoirs de l'utilisateurs 
    public function get_assign($course_id){
        $query = 'SELECT id, name FROM mdl_assign WHERE course = :course_id';
        $get_assign = $this->dbConnect->prepare($query);
        $get_assign->bindValue(':course_id', $course_id, PDO::PARAM_INT);
        $get_assign->execute();
        return $get_assign->fetchAll(PDO::FETCH_OBJ);
    }
    // récupère les quizs de l'utilisateurs 
    public function get_quiz($course_id){
        $query = 'SELECT id, name FROM mdl_quiz WHERE course = :course_id';
        $get_quiz = $this->dbConnect->prepare($query);
        $get_quiz->bindValue(':course_id', $course_id, PDO::PARAM_INT);
        $get_quiz->execute();
        return $get_quiz->fetchAll(PDO::FETCH_OBJ);
    }
}