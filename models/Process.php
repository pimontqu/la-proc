<?php

class Process extends Database{
    public $id;
    public $process;
    public $zeppelin_id;
    public $id_moodle_instance;

    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    public function get_id_by_zeppelin_id($zeppelin_id){
        $query = 'SELECT id FROM la_process WHERE zeppelin_id = :zeppelin_id';
        $get_id = $this->dbConnect->prepare($query);
        $get_id->bindValue(':zeppelin_id', $zeppelin_id, PDO::PARAM_INT);
        $get_id->execute();
        return $get_id->fetch(PDO::FETCH_OBJ)->id;
    }

    public function get_processes_by_moodle_object($moodle_object){
        $query = 'SELECT la_process.id AS id, la_process.zeppelin_id AS zeppelin_id, la_process.process AS process_name, la_moodle_instances.name AS moodle_object 
        FROM la_process 
        INNER JOIN la_moodle_instances ON la_moodle_instances.id = la_process.id_la_moodle_instances 
        WHERE la_moodle_instances.name = :moodle_object';
        $get_processes = $this->dbConnect->prepare($query);
        $get_processes->bindValue(':moodle_object', $moodle_object, PDO::PARAM_STR);
        $get_processes->execute();
        return $get_processes->fetchAll(PDO::FETCH_OBJ);
    }


    public function get_all_zeppelin_id(){
        $query = 'SELECT zeppelin_id FROM la_process';
        $get_all = $this->dbConnect->query($query);
        return $get_all->fetchAll(PDO::FETCH_COLUMN);
    }

    public function get_all_id_and_zeppelin_id(){
        $query = 'SELECT id, zeppelin_id FROM la_process';
        $get_all = $this->dbConnect->query($query);
        return $get_all->fetchAll(PDO::FETCH_OBJ);
    }

    public function get_all(){
        $query = 'SELECT la_process.id, process, la_moodle_instances.name AS moodle_type
        FROM la_process
        INNER JOIN la_moodle_instances ON la_moodle_instances.id = la_process.id_la_moodle_instances';
        $get_all = $this->dbConnect->query($query);
        return $get_all->fetchAll(PDO::FETCH_OBJ);
    }

    public function add(){
        $query = 'INSERT INTO la_process(zeppelin_id, process, id_la_moodle_instances) VALUES(:zeppelin_id, :process, :id_la_moodle_instances)';
        $add = $this->dbConnect->prepare($query);
        $add->bindValue(':zeppelin_id', $this->zeppelin_id, PDO::PARAM_STR);
        $add->bindValue(':process', $this->process, PDO::PARAM_STR);
        $add->bindValue(':id_la_moodle_instances', $this->id_moodle_instance, PDO::PARAM_INT);
        return $add->execute();
        
    }

    public function remove($id){
        $query = 'DELETE FROM la_process WHERE id = :id';
        $delete = $this->dbConnect->prepare($query);
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        $query_sub = 'DELETE FROM la_subscriptions WHERE id_la_process = :id_process';
        $delete_sub = $this->dbConnect->prepare($query_sub);
        $delete_sub->bindValue(':id_process', $id, PDO::PARAM_INT);
        $this->dbConnect->beginTransaction();
        if($delete_sub->execute()){
            if($delete->execute()){
                $this->dbConnect->commit();
                return true;
            }else{
                $this->dbConnect->rollback();
                return false;
            }
        }else{
            $this->dbConnect->rollback();
            return false;
        }
    }

    public function exist($name){
        $query = 'SELECT COUNT(*) AS exist FROM la_process WHERE process = :process';
        $exist = $this->dbConnect->prepare($query);
        $exist->bindValue(':process', $name, PDO::PARAM_STR);
        $exist->execute();
        return $exist->fetch(PDO::FETCH_OBJ)->exist;
    }
}