<?php

class Moodle_instances extends Database{
    private $id;
    private $name;

    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    public function add($name){
        $query = 'INSERT INTO la_moodle_instances(name) VALUES(:name)';
        $add = $this->dbConnect->prepare($query);
        $add->bindValue(':name', $name, PDO::PARAM_STR);
        return $add->execute();
    }

    public function get_all(){
        $query = 'SELECT id, name FROM la_moodle_instances';
        $get_all = $this->dbConnect->query($query);
        return $get_all->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function get_id($name){
        $query = 'SELECT id FROM la_moodle_instances WHERE name = :name';
        $get_id = $this->dbConnect->prepare($query);
        $get_id->bindValue(':name', $name, PDO::PARAM_INT);
        $get_id->execute();
        return $get_id->fetch(PDO::FETCH_OBJ)->id;
    }
}