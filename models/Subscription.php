<?php

class Subscription extends Database{
    private $id;
    private $id_user;
    private $id_process;

    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    // ajoute un abonnement
    public function add($user_id, $id_process, $moodle_object){
        $query = 'INSERT INTO la_subscriptions(moodle_object, id_la_process, id_la_users) VALUES(:moodle_object, :id_process, :user_id)';
        $add = $this->dbConnect->prepare($query);
        $add->bindValue(':moodle_object', $moodle_object, PDO::PARAM_INT);
        $add->bindValue(':id_process', $id_process, PDO::PARAM_INT);
        $add->bindValue(':user_id', $user_id, PDO::PARAM_INT);
        return $add->execute();
    }
    // supprime un abonnement
    public function remove($user_id, $id_process, $moodle_object){
        $query = 'DELETE FROM la_subscriptions WHERE moodle_object = :moodle_object AND id_la_process = :id_process AND id_la_users = :user_id';
        $remove = $this->dbConnect->prepare($query);
        $remove->bindValue(':moodle_object', $moodle_object, PDO::PARAM_INT);
        $remove->bindValue(':id_process', $id_process, PDO::PARAM_INT);
        $remove->bindValue(':user_id', $user_id, PDO::PARAM_INT);
        return $remove->execute();
    }
    // récupère tous les abonnement
    public function get_all(){
        $query = 'SELECT la_subscriptions.id, la_subscriptions.moodle_object, la_process.zeppelin_id, la_users.email 
        FROM la_subscriptions 
        INNER JOIN la_process ON la_process.id = la_subscriptions.id_la_process 
        INNER JOIN la_users ON la_users.id = la_subscriptions.id_la_users';
        $get = $this->dbConnect->query($query);
        return $get->fetchAll(PDO::FETCH_OBJ);
    }
    // récupère tous les abonnements d'un utilisateur a une procédure
    public function get($user_id, $moodle_type, $zeppelin_id){
        $query = 'SELECT la_subscriptions.moodle_object
        FROM la_subscriptions
        INNER JOIN la_process ON la_process.id = la_subscriptions.id_la_process 
        WHERE la_process.id_la_moodle_instances = :moodle_type AND la_subscriptions.id_la_users = :user_id AND la_process.zeppelin_id = :zeppelin_id';
        $get = $this->dbConnect->prepare($query);
        $get->bindValue(':user_id', $user_id, PDO::PARAM_INT);
        $get->bindValue(':moodle_type', $moodle_type, PDO::PARAM_INT);
        $get->bindValue(':zeppelin_id', $zeppelin_id, PDO::PARAM_STR);
        $get->execute();
        return $get->fetchAll(PDO::FETCH_COLUMN);
    }

    public function exist(){
        
    }
}