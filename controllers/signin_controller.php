<?php
session_start();
if(isset($_SESSION['user'])){
    header('location: /index.php');
    exit();
}
$server_error_message = 'Un problèmes est survenue lors connexion veuillez réessayer plus tard';
$login_error_message = 'Votre mot de passe ou email n\'est pas valide';

if(isset($_POST['loginButton'])){
    $error_message = '';
    $input_email = trim(strip_tags($_POST['email']));
    $input_pwd = trim(strip_tags($_POST['password']));
    $user = new User($ini['database']);
    if($user->exit($input_email)){
        if($user->get($input_email)){
            if(password_verify($input_pwd, $user->password)){
                $_SESSION['user'] = $user->email;
                $_SESSION['role'] = $user->role;
                $_SESSION['id'] = $user->id;
                header('location: /');
                exit();
            }else{
                $error_message = $login_error_message;
            }
        }else{
            $error_message = $server_error_message;
        }
    }else{
        $error_message = $login_error_message;
    }
    echo $error_message;
}