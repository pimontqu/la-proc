<?php
session_start();
if(!isset($_SESSION['user'])){
    header('location: /index.php?page=1');
    exit();
}else{
    // suppression d'un utilisateur
    if(isset($_POST['delete_user'])){
        include_once '../global_var.php';
        $user = new User($ini['database']);
        $user_id = $_POST['user_id'];
        $response = $user->remove($user_id);
        echo json_encode(array('input' => $_POST, 'response' => $response));
    // suppression d'une procédure
    }elseif(isset($_POST['delete_process'])){
        include_once '../global_var.php';
        $process = new Process($ini['database']);
        $process_id = $_POST['process_id'];
        $response = $process->remove($process_id);
        echo json_encode(array('input' => $_POST, 'response' => $response));
    }else{
        $user = new User($ini['database']);
        $process = new Process($ini['database']);
        if(count($_POST) > 0){
            // ajoute un utilisateur
            if(isset($_POST['add_user'])){
                $role = new Role($ini['database']);
                $user->email = trim(strip_tags($_POST['email']));
                if($user->exit($user->email)){
                    $error_add_user = 'l\'utilisateur existe déjà.';
                }else{
                    $password = $_POST['password'];
                    $password_confirmation = $_POST['confirm_password'];
                    if($password !== $password_confirmation){
                        $error_add_user = 'les deux mot de passe ne sont pas identique.';
                    }else{
                        $user->password = password_hash($password, PASSWORD_BCRYPT);
                        if(isset($_POST['is_admin'])){
                            $role->get_id('admin');
                            $user->id_roles = $role->id;
                        }else{
                            $role->get_id('user');
                            $user->id_roles = $role->id;
                        }
                        if($user->add()){
                            $valid_user_add = 'L\'utilisateur à bien été ajouté.';
                        }else{
                            $error_add_user = 'Une erreur c\'est produite lors de l\'enrgistrement, veuillez retenter plus tard.';
                        }
                    }
                }
            // ajoute une procédure
            }elseif(isset($_POST['add_process'])){
                $process->process = trim(strip_tags($_POST['process']));
                if($process->exist($process->process)){
                    $error_add_process = 'Le nom de la procédure est déjà prit. Veuillez en choisir un autre.';
                }else{
                    $process->id_moodle_instance = $_POST['moodle_type'];
                    if($process->id_moodle_instance === 0){
                        $error_add_process = 'Veuillez sélectionner un type d\'objet moodle.';
                    }else{
                        $process->zeppelin_id = $_POST['zeppelin_note'];
                        if($process->zeppelin_id === 0){
                            $error_add_process = 'Veuillez sélectionner une note zeppelin.';
                        }else{
                            if($process->add()){
                                $valid_add_process = 'La procédure à bien été ajouté.';
                            }else{
                                $error_add_process = 'Une erreur c\'est produite lors de l\'enrgistrement, veuillez retenter plus tard.';
                            }
                        }
                    }
                }
            }
        }
        $zeppelin = new Zeppelin($ini['zeppelin']);
        $moodle_instance = new Moodle_instances($ini['database']);
        $moodle_types = $moodle_instance->get_all();
        // recupère toutes les notes zeppelin, les process(id note zep et nom), utilisateurs, 
        $zeppelin_notes = $zeppelin->get_all_notes()->body;
        $all_processes_id = $process->get_all_zeppelin_id();
        $all_users = $user->get_all();
        $all_processes_with_name = $process->get_all();
    }
}
