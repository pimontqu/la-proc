<?php
session_start();
// redirection vers la page de connexion si c'est un utilisateurs non connecter
if(!isset($_SESSION['user'])){
    header('location: /index.php?page=1');
    exit();
}else{
    if(count($_POST) > 0){
        include_once '../global_var.php';
        $moodledb = new Moodledb($ini['moodle_db']);
        $subcrition = new Subscription($ini['database']);
        $zeppelin_id = $_POST['zeppelin_id'];
        $moodle_instance = new Moodle_instances($ini['database']);
        $user_email = $_SESSION['user'];
        $hash_class = new Hash_class($ini['hash']);
        $email_hashed = $hash_class->hash_mail($user_email);
        $moodle_id_user = $moodledb->get_id_user($email_hashed);
        $moodle_course = $moodledb->get_courses($moodle_id_user);
        // récupère tous les cours que l'utilisateur a la charge
        if(isset($_POST['list_courses'])){
            $moodle_type = $moodle_instance->get_id('cours');
            $process_sub = $subcrition->get($_SESSION['id'], $moodle_type, $zeppelin_id);
            echo json_encode(array('courses' => $moodle_course, 'zeppelin_id' => $zeppelin_id, 'process_sub' => $process_sub));
        // récupère tous les devoir que l'utilisateur a la charge
        }elseif(isset($_POST['list_assigns'])){
            $all_asigns = array();
            foreach ($moodle_course as $course) {
                $id_course = $course->course_id;
                $assign_in_course = $moodledb->get_assign($id_course);
                if(count($assign_in_course) > 0){
                    $all_asigns = array_merge($all_asigns, $assign_in_course);
                }
            }
            $moodle_type = $moodle_instance->get_id('devoir');
            $process_sub = $subcrition->get($_SESSION['id'], $moodle_type, $zeppelin_id);
            echo json_encode(array('assigns' => $all_asigns, 'zeppelin_id' => $zeppelin_id, 'process_sub' => $process_sub));
        // récupère tous les quiz que l'utilisateur a la charge
        }elseif(isset($_POST['list_quizs'])){
            $all_quizs = array();
            foreach ($moodle_course as $course) {
                $id_course = $course->course_id;
                $quiz_in_course = $moodledb->get_quiz($id_course);
                if(count($quiz_in_course) > 0){
                    $all_quizs = array_merge($all_quizs, $quiz_in_course);
                }
            }
            $moodle_type = $moodle_instance->get_id('quiz');
            $process_sub = $subcrition->get($_SESSION['id'], $moodle_type, $zeppelin_id);
            echo json_encode(array('quizs' => $all_quizs, 'zeppelin_id' => $zeppelin_id, 'process_sub' => $process_sub));
        // enregistre l'abonnement au procédure
        }else{
            $process = new Process($ini['database']);
            $checked = (int)$_POST['checked'];
            $moodle_object = (int)$_POST['moodle_object'];
            $id_process = $process->get_id_by_zeppelin_id($zeppelin_id);
            if($checked){
                $action = 'enregistrement';
                if($subcrition->add($_SESSION['id'], $id_process, $moodle_object)){
                    $response = 1;
                }else{
                    $response = 0;
                }
            }else{
                $action = 'suppresion';if($subcrition->remove($_SESSION['id'], $id_process, $moodle_object)){
                    $response = 1;
                }else{
                    $response = 0;
                }
            }
            echo json_encode(array('input' => $_POST, 'action' => $action, 'response' => $response));
        }
    }else{
        $process = new Process($ini['database']);
        $course_processes = $process->get_processes_by_moodle_object('cours');
        $assign_processes = $process->get_processes_by_moodle_object('devoir');
        $quiz_processes = $process->get_processes_by_moodle_object('quiz');
    }
    
}