<?php
include_once '../global_var.php';
// recupère les paramètres cli
$arg = getopt('e:p:');
$user = new User($ini['database']);
$role = new Role($ini['database']);
$role_name  = 'admin';
// assign le role admin
if($role->exist($role_name)){
    $role->get_id($role_name);
    // verifie si l'utilisateur n'existe pas et l'enregistre
    if(!$user->exit($arg['e'])){
        $user->password = password_hash($arg['p'], PASSWORD_BCRYPT);
        $user->email = $arg['e'];
        $user->id_roles = $role->id;
        if(!$user->add()){
            echo 'erreur pendant l\'enregistrement de l\'utilisateur' . PHP_EOL;
        }
    }else{
        echo 'l\'utilisateur existe déjà' . PHP_EOL;
    }
}else{
    echo 'Ce role n\'existe pas' . PHP_EOL;
}
