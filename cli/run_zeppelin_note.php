<?php

include_once '../global_var.php';

$subscription = new Subscription($ini['database']);
$process = new Process($ini['database']);
$zeppelin = new Zeppelin($ini['zeppelin']);
$log_writer = new Log_writer('../logs/', 'cli');
$path_finsh_file = '/home/p3-scripts/file_zeppelin/finish.txt';
// pour chaque abonnement
$all_subscription = $subscription->get_all();
foreach ($all_subscription as $c_sub) {
    // ajoute un paragraphe au début de la note avec les paramtre suivant (l'id de l'objet moodle(cours,quiz, etc) et le mails de l'abonné)
    $response_add = $zeppelin->add_paragraph($c_sub->zeppelin_id, $c_sub->moodle_object, $c_sub->email);
    if($response_add->status === 'OK'){
        // execute le script zeppelin de l'abonnement
        $response_run = $zeppelin->run_all($c_sub->zeppelin_id);
        $finish_file_exist = file_exists($path_finsh_file);
        // regarde si le fichier de fin de script est bien créer
        while(!$finish_file_exist){
            $finish_file_exist = file_exists($path_finsh_file);
        }
        if($finish_file_exist){
            unlink($path_finsh_file);
        }
        if($response_run->status === 'OK'){
            // recupère les infos du premier paragraphe de script zeppelin (celui des paramatres)
            $first_paragraph_info = $zeppelin->get_first_paragraph_info($c_sub->zeppelin_id);
            // supprime le premier paragraphe
            $response_delete = $zeppelin->delete_paragraph($c_sub->zeppelin_id, $first_paragraph_info->id);
            while($response_delete->status === 'OK'){
                $response_delete = $zeppelin->delete_paragraph($c_sub->zeppelin_id, $first_paragraph_info->id);
            }
        }else{
            $log_writer->write('le paragraphe des paramatres n\'as pas pue être ajouté pour cette abonnement : ' . $c_sub->id);
            continue;
        }
    }else{
        $log_writer->write('une erreur c\'est produite lors de l\'execution de cette note zeppelin : ' . $c_sub->zeppelin_id);
        continue;
    }
}