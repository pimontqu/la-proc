<?php
include_once 'assets/classes/Database.php';
include_once 'models/Process.php';
include_once 'models/Subscription.php';
include_once 'models/User.php';
include_once 'models/Moodle_instance.php';
include_once 'models/Role.php';
include_once 'models/Moodledb.php';
include_once 'assets/classes/Zeppelin.php';
include_once 'assets/classes/Hash_class.php';
include_once 'assets/classes/Log_writer.php';


$ini = parse_ini_file('config.ini', true);