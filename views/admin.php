<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Panneaux d'administration</h1>
        <a href="/index.php">Acceuil</a>
        <h2>ajout d'utilisateur</h2>
        <?php
        if(isset($error_add_user)){
            ?>
            <div class="alert alert-danger" role="alert">
                <p><?= $error_add_user ?></p>
            </div>
            <?php
        }
        if(isset($valid_user_add)){
            ?>
            <div class="alert alert-success" role="alert">
                <p><?= $valid_user_add ?></p>
            </div>
            <?php
        }
        ?>
        <div class="col-md-6">
            <form action="/index.php?page=2" method="post">
                <div class="form-group">
                    <label for="email">Addresse email</label>
                    <input type="email" name="email" class="form-control" id="email" value="<?= (isset($user)? $user->email : '') ?>" required/>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" class="form-control" id="password" required/>
                </div>
                <div class="form-group">
                    <label for="password">Confirmation du mot de passe</label>
                    <input type="password" name="confirm_password" class="form-control" id="password" required/>
                </div>
                <div class="form-check">
                    <input type="checkbox" name="is_admin" class="form-check-input" id="is_admin" <?= (isset($_POST['is_admin'])? 'checked' : '') ?>/>
                    <label class="form-check-label" for="is_admin">Administrateur</label>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary" name="add_user">Submit</button>
                </div>
            </form>
        </div>
        <h2 >ajout procédures</h2>
        <?php
        if(isset($error_add_process)){
            ?>
            <div class="alert alert-danger" role="alert">
                <p><?= $error_add_process ?></p>
            </div>
            <?php
        }
        if(isset($valid_add_process)){
            ?>
            <div class="alert alert-success" role="alert">
                <p><?= $valid_add_process ?></p>
            </div>
            <?php
        }
        ?>
        <div class="col-md-6">
            <form action="/index.php?page=2" method="post">
                <div class="form-group">
                    <label for="process">Nom de la procédure</label>
                    <input type="text" name="process" class="form-control" id="process" value="<?= ($process->process !== null ? $process->process : '') ?>" required/>
                </div>
                <div class="form-group">
                    <label for="zeppelin_note">Note Zeppelin</label>
                    <select class="custom-select" name="zeppelin_note" id="zeppelin_note">
                        <option value="0" selected>Veuillez selectionnez une option</option>
                        <?php
                        foreach ($zeppelin_notes as $note) {
                            if(!in_array($note->id, $all_processes_id)){
                            ?>
                            <option value="<?= $note->id ?>"><?= $note->name ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="moodle_type">Type d'objet moodle</label>
                    <select class="custom-select" name="moodle_type" id="moodle_type">
                        <option value="0" selected>Veuillez selectionnez une option</option>
                        <?php
                        foreach ($moodle_types as $type) {
                            ?>
                            <option value="<?= $type->id ?>"><?= $type->name ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary" name="add_process">Submit</button>
                </div>
            </form>
        </div>
        <h2>liste des utilisateurs</h2>
        <div class="col-md-12">
            <div id="feedback_del_user"></div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($all_users as $user) {
                        ?>
                        <tr class="<?= str_replace('@', '_', str_replace('.', '_', $user->email)) ?>">
                            <td scope="col"><?= $user->email ?></td>
                            <td scope="col"><?= $user->user_role ?></td>
                            <td scope="col"><button class="delete_user <?= str_replace('@', '_', str_replace('.', '_', $user->email)) ?>" id="<?= $user->user_id ?>">Supprimer</button></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <h2>liste des procédures</h2>
        <div class="col-md-12">
            <div id="feedback_del_proc"></div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nom</th>
                        <th scope="col">Type d'objet moodle</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($all_processes_with_name as $recorded_process) {
                        ?>
                        <tr class="<?= str_replace(' ', '_', $recorded_process->process) ?>">
                            <td scope="col"><?= $recorded_process->process ?></td>
                            <td scope="col"><?= $recorded_process->moodle_type ?></td>
                            <td scope="col"><button class="delete_process <?= str_replace(' ', '_', $recorded_process->process) ?>" id="<?= $recorded_process->id ?>">Supprimer</button></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="assets/js/admin.js"></script>