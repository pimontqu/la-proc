<div class='row'>
    <div class="col-md-12">
        <?php
        if($_SESSION['role'] == 'admin'){
        ?>
        <a href="/index.php?page=2">administration</a>
        <?php
        }
        ?>
        <a href="/index.php?page=3">Déconnexion</a>
        <h1>Liste des procédure</h1>
        <h2>Procédure pour les cours</h2>
        <ul class="list-unstyled">
        <?php
        foreach ($course_processes as $course_process) {
            ?>
            <li><button type="button" class="btn btn-primary modal_opener <?= $course_process->zeppelin_id ?> course_process" data-toggle="modal" data-target="#exampleModal"><?= $course_process->process_name ?></button></li>
            <?php
        }
        ?>
        </ul>
        <h2>Procédure pour les devoirs</h2>
        <ul class="list-unstyled">
        <?php
        foreach ($assign_processes as $assign_process) {
            ?>
            <li><button type="button" class="btn btn-primary modal_opener <?= $assign_process->zeppelin_id ?> assign_process" data-toggle="modal" data-target="#exampleModal"><?= $assign_process->process_name ?></button></li>
            <?php
        }
        ?>
        </ul>
        <h2>Procédure pour les quizs</h2>
        <ul class="list-unstyled">
        <?php
        foreach ($quiz_processes as $quiz_process) {
            ?>
            <li><button type="button" class="btn btn-primary modal_opener <?= $quiz_process->zeppelin_id ?> quiz_process" data-toggle="modal" data-target="#exampleModal"><?= $quiz_process->process_name ?></button></li>
            <?php
        }
        ?>
        </ul>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body">
                <ul>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/home.js"></script>