<div class="row">
    <div class="col-md-8 offset-2">
        <form action="/index.php?page=1" method="post">
            <div class="form-group">
                <label for="email">Addresse email</label>
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" required>
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="password">Mot de passe</label>
                <input type="password" name="password" class="form-control" id="password" required>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary" name="loginButton">Submit</button>
            </div>
        </form>
    </div>
</div>