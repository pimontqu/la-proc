// requête ajax qui permet de s'abonner au procédure
function subscribe(element) {
    var checked = (element.checked ? 1 : 0);
    $.post('/controllers/home_controller.php',{
        checked : checked,
        zeppelin_id : document.getElementById('zeppelin_id').innerText,
        moodle_object :  parseInt(element.id),
    },
    function(data){
        var response = data.response;
        var modal_body = $('#modal-body');
        if(response === 1){
            modal_body.prepend('<div class="alert alert-success" id="feedback_sub" role="alert"><p>Votre modification à bien été prix en compte<p></div>');
            setTimeout(function() {
                $('#feedback_sub').remove();
            }, 2000);
        }else{
            modal_body.prepend('<div class="alert alert-danger" id="feedback_sub" role="alert"><p>Une erreur c\'est produite<p></div>');
            setTimeout(function() {
                $('#feedback_sub').remove();
            }, 2000);
        }
    }, 'json');
}

// a l'ouverture de la modal d'abonnement au procédure récupère et liste tous les objets moodle compatible avec le process
$('.modal_opener').on('click', function () {
    var classes = $(this).attr('class').split(' ');
    var process_class = classes[classes.length - 1];
    var zeppelin_id = classes[classes.length - 2];
    //console.log(zeppelin_id);
    if(process_class === 'course_process'){
        $.post('/controllers/home_controller.php',{
            list_courses : 1,
            zeppelin_id : zeppelin_id,
        },
        function(data){
            var courses = data.courses;
            var note_zeppelin = data.zeppelin_id;
            var subscription = data.process_sub;
            var html = '<p class="d-none" id="zeppelin_id">' + note_zeppelin + '</p>\n\t<ul>';
            courses.forEach(course => {
                html += '\t\t<li><label for="' + course.course_id + '">' + course.course_name + '</label><input type="checkbox" name="' + course.course_id 
                + '" id="' + course.course_id + '"' + (subscription.indexOf(parseInt(course.course_id)) != -1? 'checked=""' : '') 
                + ' onclick="subscribe(this)"></li>\n';
            });
            html += '</ul>';
            $('#modal-body').children().remove();
            $('#modal-body').append(html);
        }, 'json');
    }else if(process_class === 'assign_process'){
        $.post('/controllers/home_controller.php',{
            list_assigns : 1,
            zeppelin_id : zeppelin_id,
        },
        function(data){
            var assigns = data.assigns;
            var note_zeppelin = data.zeppelin_id;
            var subscription = data.process_sub;
            var html = '<p class="d-none" id="zeppelin_id">' + note_zeppelin + '</p>\n\t<ul>';
            assigns.forEach(assign => {
                html += '\t\t<li><label for="' + assign.id + '">' + assign.name + '</label><input type="checkbox" name="' + assign.id + '" id="' + assign.id 
                + '"' + (subscription.indexOf(parseInt(assign.id)) != -1? 'checked=""' : '') + ' onclick="subscribe(this)"></li>\n';
            });
            html += '</ul>';
            $('#modal-body').children().remove();
            $('#modal-body').append(html);
        }, 'json');
    }else{
        $.post('/controllers/home_controller.php',{
            list_quizs : 1,
            zeppelin_id : zeppelin_id,
        },
        function(data){
            var quizs = data.quizs;
            var note_zeppelin = data.zeppelin_id;
            var subscription = data.process_sub;
            var html = '<p class="d-none" id="zeppelin_id">' + note_zeppelin + '</p>\n\t<ul>';
            quizs.forEach(quiz => {
                html += '\t\t<li><label for="' + quiz.id + '">' + quiz.name + '</label><input type="checkbox" name="' + quiz.id + '" id="' + quiz.id 
                + '"' + (subscription.indexOf(parseInt(quiz.id)) != -1? 'checked=""' : '') + ' onclick="subscribe(this)"></li>\n';
            });
            html += '</ul>';
            $('#modal-body').children().remove();
            $('#modal-body').append(html);
        }, 'json');
    }
    
});