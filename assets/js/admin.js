// requête ajax pour supprimer un utilisateur
$('.delete_user').on('click', function () {
    var classes = $(this).attr('class').split(' ');
    var user_email = classes[classes.length - 1];
    $.post('/controllers/admin_controller.php', {
        delete_user : 1,
        user_id : $(this).attr('id'),
        user_email : user_email
    },
    function(data) {
        var class_parent = data.input.user_email;
        var parent = $('tr.' + class_parent);
        var response = data.response;
        var feedback_div = $('#feedback_del_user');
        feedback_div.children().remove();
        if(response){
            parent.remove();
            feedback_div.append('<div class="alert alert-success" role="alert"><p>L\'utilisateur a bien été supprimé<p></div>');
        }else{
            feedback_div.append('<div class="alert alert-danger" role="alert"><p>Une erreur c\'est produite lor de la supréssion de L\'utilisateur<p></div>')
        }
        console.log(data);
    }, 'json');
});

// requête ajax pour supprimer une procédure
$('.delete_process').on('click', function () {
        var classes = $(this).attr('class').split(' ');
        var process_name = classes[classes.length - 1];
    $.post('/controllers/admin_controller.php', {
        delete_process : 1,
        process_id : $(this).attr('id'),
        process_name : process_name
    },
    function(data) {
        var class_parent = data.input.process_name;
        var parent = $('tr.' + class_parent);
        var response = data.response;
        var feedback_div = $('#feedback_del_proc');
        feedback_div.children().remove();
        if(response){
            parent.remove();
            feedback_div.append('<div class="alert alert-success" role="alert"><p>La procédure a bien été supprimé<p></div>');
        }else{
            feedback_div.append('<div class="alert alert-danger" role="alert"><p>Une erreur c\'est produite lor de la supréssion de la procédure<p></div>')
        }
        console.log(data);
    }, 'json');
});