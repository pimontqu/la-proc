-- object: public.la_process | type: TABLE --
-- DROP TABLE IF EXISTS public.la_process CASCADE;
CREATE TABLE public.la_process (
	id BIGSERIAL NOT NULL,
	zeppelin_id varchar(10) NOT NULL,
	process varchar(50) NOT NULL,
	id_la_moodle_instances smallint NOT NULL,
	CONSTRAINT la_process_pk PRIMARY KEY (id),
	CONSTRAINT unique_process UNIQUE (zeppelin_id,process)

);
-- ddl-end --
ALTER TABLE public.la_process OWNER TO laproc_user;
-- ddl-end --

-- object: public.la_users | type: TABLE --
-- DROP TABLE IF EXISTS public.la_users CASCADE;
CREATE TABLE public.la_users (
	id BIGSERIAL NOT NULL,
	email varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	password_reset bool NOT NULL,
	id_la_roles smallint NOT NULL,
	CONSTRAINT la_users_pk PRIMARY KEY (id),
	CONSTRAINT unique_user UNIQUE (email)

);
-- ddl-end --
ALTER TABLE public.la_users OWNER TO laproc_user;
-- ddl-end --

-- object: public.la_roles | type: TABLE --
-- DROP TABLE IF EXISTS public.la_roles CASCADE;
CREATE TABLE public.la_roles (
	id SERIAL NOT NULL,
	name varchar(10) NOT NULL,
	CONSTRAINT roles_pk PRIMARY KEY (id),
	CONSTRAINT unique_role UNIQUE (name)

);
-- ddl-end --
ALTER TABLE public.la_roles OWNER TO laproc_user;
-- ddl-end --

-- object: la_roles_fk | type: CONSTRAINT --
-- ALTER TABLE public.la_users DROP CONSTRAINT IF EXISTS la_roles_fk CASCADE;
ALTER TABLE public.la_users ADD CONSTRAINT la_roles_fk FOREIGN KEY (id_la_roles)
REFERENCES public.la_roles (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.la_moodle_instances | type: TABLE --
-- DROP TABLE IF EXISTS public.la_moodle_instances CASCADE;
CREATE TABLE public.la_moodle_instances (
	id SERIAL NOT NULL,
	name varchar(50) NOT NULL,
	CONSTRAINT moodle_instances_pk PRIMARY KEY (id),
	CONSTRAINT unique_instance UNIQUE (name)

);
-- ddl-end --
ALTER TABLE public.la_moodle_instances OWNER TO laproc_user;
-- ddl-end --

-- object: la_moodle_instances_fk | type: CONSTRAINT --
-- ALTER TABLE public.la_process DROP CONSTRAINT IF EXISTS la_moodle_instances_fk CASCADE;
ALTER TABLE public.la_process ADD CONSTRAINT la_moodle_instances_fk FOREIGN KEY (id_la_moodle_instances)
REFERENCES public.la_moodle_instances (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.la_subscriptions | type: TABLE --
-- DROP TABLE IF EXISTS public.la_subscriptions CASCADE;
CREATE TABLE public.la_subscriptions (
	id BIGSERIAL NOT NULL,
	moodle_object smallint NOT NULL,
	id_la_process bigint,
	id_la_users bigint,
	CONSTRAINT la_subscription_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.la_subscriptions OWNER TO laproc_user;
-- ddl-end --

-- object: la_process_fk | type: CONSTRAINT --
-- ALTER TABLE public.la_subscriptions DROP CONSTRAINT IF EXISTS la_process_fk CASCADE;
ALTER TABLE public.la_subscriptions ADD CONSTRAINT la_process_fk FOREIGN KEY (id_la_process)
REFERENCES public.la_process (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: la_users_fk | type: CONSTRAINT --
-- ALTER TABLE public.la_subscriptions DROP CONSTRAINT IF EXISTS la_users_fk CASCADE;
ALTER TABLE public.la_subscriptions ADD CONSTRAINT la_users_fk FOREIGN KEY (id_la_users)
REFERENCES public.la_users (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


