<?php


class Hash_class{

    private $salt;

    public function __construct($info_hash){
        $this->salt = $info_hash['salt'];
    }

    public function hash_mail($email){
        return hash_hmac('sha256', $email, $this->salt) . '@mailbidon.fr';
    }
}