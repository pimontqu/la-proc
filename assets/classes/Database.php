<?php

/*
 * classe qui represente la base de données 
 */
class dataBase {

    /**
     * ces variables contiendront toutes les informations pour la connexion à la base données 
     * @var type 
     */
    protected $host;
    protected $pwd;
    protected $login;
    protected $db;
    protected $dbConnect;

    /**
     * recupère toutes les informations nécessaire pour la connexion et ce que connecte à la base de données
     */
    protected function __construct($db_info) {
        $this->host = $db_info['host'];
        $this->db = $db_info['name'];
        $this->login = $db_info['user'];
        $this->pwd = $db_info['pwd'];
        try {
            $this->dbConnect = new \PDO('pgsql:host=' . $this->host . ';dbname=' . $this->db . ';', $this->login, $this->pwd);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
