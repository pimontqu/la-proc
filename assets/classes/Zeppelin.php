<?php

class Zeppelin{
    private $login;
    private $pwd;
    private $end_point = 'http://lap3-1.univ-lille.fr/api/';

    public function __construct($login_info)
    {
        $this->login = $login_info['login'];
        $this->pwd = $login_info['pwd'];
    }
    // recupère le cookie de connexion pour l'ajouter au header des requêtes faite à l'api
    public function connection(){
        $post_data = http_build_query(
            array(
                'userName' => $this->login,
                'password' => $this->pwd
            )
        );
        $url = $this->end_point . 'login';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        preg_match_all('/JSESSIONID=[a-zA-Z0-9-]+; Path=\/; HttpOnly/', $result, $cookie_connection);
        return $cookie_connection[0][count($cookie_connection[0]) - 1];
    }
    // crée le header pour les requête
    public function make_header($method, $cookie_connection){
        $options = array(
            'http' => array(
                'method'  => $method,
                'header'  => 'Cookie: ' . $cookie_connection . '\r\n',
            )
        );
        return stream_context_create($options);
    }
    // execute tous les paragraphes d'une note zeppelin
    public function run_all($id_note){
        $cookie_connection = $this->connection();
        $url = $this->end_point . 'notebook/job/' . $id_note;
        $context = $this->make_header('POST', $cookie_connection);
        //2FX5Q85XM
        return json_decode(file_get_contents($url , false, $context));
    }
    // ajoute un paragraphe au avec les paramètre utile au script zeppelin a son début
    public function add_paragraph($id_note, $moodle_object, $email){
        $post_data = json_encode(
            array(
                'title' => 'api param',
                'text' => '%python' . PHP_EOL . 'moodle_object = ' . $moodle_object . PHP_EOL . 'api_email = \'' . $email . '\'',
                'index' => 0
            )
        );
        $cookie_connection = $this->connection();
        $url = $this->end_point . 'notebook/' . $id_note . '/paragraph';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie_connection);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
    // récupère les infos (titre, id, text, etc) du premier paragraph
    public function get_first_paragraph_info($id_note){
        $cookie_connection = $this->connection();
        $url = $this->end_point . 'notebook/' . $id_note;
        $context = $this->make_header('GET', $cookie_connection);
        //2FX5Q85XM
        return json_decode(file_get_contents($url , false, $context))->body->paragraphs[0];
    }
    // supprime une paragraphe
    public function delete_paragraph($id_note, $id_paragraph){
        $cookie_connection = $this->connection();
        $url = $this->end_point . 'notebook/' . $id_note . '/paragraph/' . $id_paragraph;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie_connection);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        return json_decode($result);

    }
    // récupère toutes les notes.
    public function get_all_notes(){
        $cookie_connection = $this->connection();
        $url = $this->end_point . 'notebook';
        $context = $this->make_header('GET', $cookie_connection);
        return json_decode(file_get_contents($url , false, $context));

    }
}