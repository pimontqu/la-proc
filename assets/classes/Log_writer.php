<?php

class Log_writer{
    private $prefix_filename;
    private $log_dir;
    
    public function __construct($log_dir, $prefix_filename) {
        if($log_dir[strlen($log_dir) - 1] !== '/'){
            $log_dir .= '/';
        }
        $this->log_dir = $log_dir;
        $this->prefix_filename = $prefix_filename;
    }
    // ecrit dans le fichier de logs et créer le dossier et/ou le fichier si il n'existe pas
    public function write($message){
        $datetime = new DateTime();
        $day_format = $datetime->format('d-m-y');
        if(!is_dir($this->log_dir)){
            mkdir($this->log_dir);
        }
        $hour_format = $datetime->format('G:i:s');
        $log_path = $this->log_dir . $this->prefix_filename . $day_format . '.log';
        $writer = fopen($log_path, 'a');
        fwrite($writer ,'[' . $hour_format . '] ' . $message . PHP_EOL);
    }
}