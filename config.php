<?php
include_once 'global_var.php';
$page_view;
$controller;
if(isset($_GET['page']) && $_GET['page'] == 1){
    $controller = 'controllers/signin_controller.php';
    $page_view = 'views/signin.php';
    $page_title = 'Connexion';
}elseif(isset($_GET['page']) && $_GET['page'] == 2){
    $controller = 'controllers/admin_controller.php';
    $page_view = 'views/admin.php';
    $page_title = 'Administration';
}elseif(isset($_GET['page']) && $_GET['page'] == 3){
    $controller = 'controllers/deconnection_controller.php';
}else{
    $controller = 'controllers/home_controller.php';
    $page_view = 'views/home.php';
    $page_title = 'Accueil';
}